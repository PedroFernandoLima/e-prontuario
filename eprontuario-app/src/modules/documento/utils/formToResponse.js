export const formToResponse = (
  itensDocumentoPreenchido,
  itemDocumento
) => {
  console.log(itemDocumento)
  for (const key in itensDocumentoPreenchido) {
    decideQualTipoEh(itensDocumentoPreenchido[key])
  }
}

export const decideQualTipoEh = (valor) => {
  if (ehTipoBoolean(valor)) {
    console.log('Tipo Boolean')
    return 'BOOLEAN'
  }
  if (ehTipoString(valor)) {
    console.log('Tipo String')
    return 'STRING'
  }
  if (ehTipoData(valor)) {
    console.log('Tipo Hora')
    return 'DATA_HORA'
  }
}

export const ehTipoBoolean = value => {
  return typeof value === 'boolean'
}

export const ehTipoData = (value) => {
  const date = new Date(value)
  return !isNaN(date.getTime());
}

export const ehTipoString = value => {
  return typeof value === 'string'
}


/*
{
  "descricao": "string",
  "academico": {
    "id": 0,
    "nome": "string",
    "rga": "string"
  },
  "entrada": {
    "dataHora": "2021-03-25T00:05:28.772Z",
    "id": 0,
    "paciente": {
      "cpf": "string",
      "genero": "MASCULINO",
      "id": 0,
      "nome": "string"
    },
    "responsavel": {
      "contato": "string",
      "id": 0,
      "nome": "string"
    }
  },
  "id": 0,
  "itensDocumento": [
    {
      "documento": {
        "descricao": "string",
        "academico": {
          "id": 0,
          "nome": "string",
          "rga": "string",
          "tipoPessoa": "ENFERMEIRO"
        },
        "entrada": {
          "dataHora": "2021-03-25T00:05:28.772Z",
          "descricao": "string",
          "id": 0,
          "paciente": {
            "cpf": "string",
            "genero": "MASCULINO",
            "id": 0,
            "nome": "string",
            "tipoPessoa": "ENFERMEIRO"
          },
          "responsavel": {
            "contato": "string",
            "id": 0,
            "nome": "string",
            "tipoPessoa": "ENFERMEIRO"
          }
        },
        "id": 0,
        "paciente": {
          "cpf": "string",
          "genero": "MASCULINO",
          "id": 0,
          "nome": "string",
          "tipoPessoa": "ENFERMEIRO"
        },
        "titulo": "string"
      },
      "id": 0,
      "idPai": 0,
      "situacao": "ATIVO",
      "tipoItem": "STRING",
      "titulo": "string",
      "valorData": "2021-03-25T00:05:28.772Z",
      "valorFixo": {
        "chave": "string",
        "id": 0,
        "valor": "string"
      },
      "valorFlutuante": 0,
      "valorInteiro": 0,
      "valorTexto": "string"
    }
  ],
  "paciente": {
    "cpf": "string",
    "genero": "MASCULINO",
    "id": 0,
    "nome": "string"
  },
  "responsavel": {
    "contato": "string",
    "id": 0,
    "nome": "string",
    "tipoPessoa": "ENFERMEIRO"
  },
  "titulo": "string"
}
 */

