export {addPaciente} from './addPacienteApi'
export {editPaciente} from './editPaciente';
export {getPacienteFromId} from './getPacienteFromId';
export {removePaciente} from './removePaciente';
export {getPacientesFromApi} from './getPacientesFromApi';
