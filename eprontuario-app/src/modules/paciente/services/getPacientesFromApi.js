import axios from "axios";

export const getPacientesFromApi = (page = 0, limit = 10) => {
  return axios
    .get(
      `http://localhost:8080/eprontuario-api/paciente?page=${page}&size=${limit}`
    )
    .then(({ data }) => data)
    .catch(err => err);
};
